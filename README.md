# Learn Modern JavaScript by Coding a Snake Game

# start project
```
npm init -y 
```

## Express is a minimal and flexible node.js web application framework
```
npm install express --save
```
You can copy webserver: https://expressjs.com/en/starter/hello-world.html
```
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
```

## CDN PHASER

https://cdnjs.com/libraries/phaser and copy https://cdnjs.cloudflare.com/ajax/libs/phaser/3.19.0/phaser-arcade-physics.min.js

### SnakeGame

![Screenshot](snakegame.png)