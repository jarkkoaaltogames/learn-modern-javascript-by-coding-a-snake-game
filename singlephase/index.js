const express = require('express'); // pull express module
const app = express();
const port = 3000; // Specified port

app.use(express.static(__dirname + '/public/'));

// app.get('/', (req, res) => res.send('Hello World!')) // routing
app.get('/', (req, res) => res.sendFile('index.html')); // routing

app.listen(port, () => console.log(`Example app listening on port ${port}!`)) 